# Services Collection

## Useful services required for development including mysql, postgres, redis, rabbitmq, elastic search.

### Installation steps.
 - Clone
 - Run `cd servers`
 - Run `docker-compose up -d` (to start all containers) OR `docker-compose up -d serviceName` (to start a container). This will download dependencies and start the instances.